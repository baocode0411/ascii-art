package artist

import (
	"errors"
	"os"
	"strconv"
	"strings"
)

var letters []string

// StringToArt returns an art version of the string
//
// Takes as an arguments a source string and path to the banner file (which contains ascii art for each letter)
//
// Works only with \n line separators. Each letter is 8 lines height. Empty lines are just "\n"
func StringToArt(source string, bannerFilePath string) (string, error) {
	bannerBytes, err := os.ReadFile(bannerFilePath)
	if err != nil {
		return "", err
	}

	bannerString := string(bannerBytes)

	// Fixes all DOS line separators in banner file by removing all '\r' (\r\n -> \n)
	bannerString = strings.ReplaceAll(bannerString, "\r", "")

	// Removing first line because it's empty
	bannerString = bannerString[1:]

	// Splitting banner by letters
	// Each element of slice is an ascii art of letter:
	// letters[1], rune(1) = '!' -> letters[1] is an ascii art for rune '!'
	letters = strings.Split(bannerString, "\n\n")

	// Splitting source string to lines and calling lineToArt for each line
	lines := strings.Split(source, "\n")
	for i := range lines {
		lines[i], err = lineToArt(lines[i])
		if err != nil {
			return "", err
		}
	}

	// first \n should not be printed
	if lines[0] == "\n" {
		lines[0] = ""
	}

	return strings.Join(lines, ""), nil
}

// lineToArt returns an art created from a line (flat string without \n)
//
// Each line of the art is ended by \n
func lineToArt(source string) (string, error) {
	if source == "" {
		return "\n", nil
	}

	// If source is not empty, the height of art is always 8
	res := make([]string, 8)

	for _, r := range []rune(source) {
		runeArt, err := runeToArt(r)
		if err != nil {
			return "", err
		}
		art := strings.Split(runeArt, "\n")
		for i := 0; i < 8; i++ {
			res[i] += art[i]
		}
	}

	return strings.Join(res, "\n") + "\n", nil
}

// runeToArt returns as a string an 8 line height art for rune passed as an argument
func runeToArt(source rune) (string, error) {
	if source == '\n' {
		return "\n", nil
	}
	if int(source-' ') > len(letters)-1 {
		return "", errors.New("ERROR: '" + string(source) + "' (code " + strconv.Itoa(int(source)) + ") is not an ASCII character")
	}
	return letters[source-' '], nil
}
