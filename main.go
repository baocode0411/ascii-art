package main

import (
	. "ascii-art/artist"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal("Wrong number of arguments\nCorrect usage: go run . [STRING]")
	}

	source := os.Args[1]

	// Fixing cases when \n is not a new line, but just '\' and 'n'
	if !strings.Contains(source, "\n") {
		source = strings.ReplaceAll(source, "\\n", "\n")
	}

	res, err := StringToArt(source, "artist/banners/standard.txt")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(res)
}
