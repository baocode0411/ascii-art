#!/bin/bash

go run . "Hello World" | cat -e

echo "Hello\n"
go run . "Hello\n" | cat -e

echo "hello"
go run . "hello" | cat -e

echo "HeLlO"
go run . "HeLlO" | cat -e

echo "Hello There"
go run . "Hello There" | cat -e

echo "1Hello 2There"
go run . "1Hello 2There" | cat -e

echo "{Hello There}"
go run . "{Hello There}" | cat -e

echo "Hello\nThere"
go run . "Hello\nThere" | cat -e

echo "hello"
go run . "hello" | cat -e

echo "HELLO"
go run . "HELLO" | cat -e

echo "HeLlo HuMaN"
go run . "HeLlo HuMaN" | cat -e

echo "1Hello 2There"
go run . "1Hello 2There" | cat -e

echo "Hello\nThere"
go run . "Hello\nThere" | cat -e

echo "Hello\n\nThere"
go run . "Hello\n\nThere" | cat -e

echo "{Hello & There #}"
go run . "{Hello & There #}" | cat -e

echo "hello There 1 to 2!"
go run . "hello There 1 to 2!" | cat -e

echo "MaD3IrA&LiSboN"
go run . "MaD3IrA&LiSboN" | cat -e

echo "1a\"#FdwHywR&/()="
go run . "1a\"#FdwHywR&/()=" | cat -e

echo "{|}~"
go run . "{|}~" | cat -e

echo "[\\]^_ 'a"
go run . "[\\]^_ 'a" | cat -e

echo "RGB"
go run . "RGB" | cat -e

echo ":;<=>?@"
go run . ":;<=>?@" | cat -e

echo "\\!\" #$%&'()*+"
go run . "\\!\" #$%&'()*+" | cat -e

echo "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
go run . "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | cat -e

echo "abcdefghijklmnopqrstuvwxyz"
go run . "abcdefghijklmnopqrstuvwxyz" | cat -e

echo "sSjsadAJKDS1832"
go run . "sSjsadAJKDS1832" | cat -e

echo "sdfhkjdsdsKJS DHFKJ123"
go run . "sdfhkjdsdsKJS DHFKJ123" | cat -e

echo "GHJ^*&*@#"
go run . "GHJ^*&*@#" | cat -e

echo "^%D^SFBSdf173 1236"
go run . "^%D^SFBSdf173 1236" | cat -e

